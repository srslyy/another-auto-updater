﻿#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseUpx=y
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#include <GUIConstantsEx.au3>
#include <FileConstants.au3>

Global $configFile = "starter.ini"

Func config($type, $name)
	Return IniRead($configFile, $type, $name, "")
EndFunc

Func prepareConfig()
	Global $workDir = config("local", "workDir")
	Global $progDir = $workDir & config("local", "progDir")
	Global $newProgDir = $workDir & config("local", "newProgDir")

	Global $serverDriver = config("server", "driver")
	Global $serverPath = config("server", "path")
	Global $serverProgPath = config("server", "progPath")
EndFunc

Func mountServer()
	; provent driver number accupied by others
	unmountServer()

	$cmd = "cmd /c net use " & $serverDriver & " " & $serverPath
	$cmd = $cmd & " " & config("server", "password") & " /user:" & config("server", "user")
	RunWait($cmd, "", @SW_HIDE)
EndFunc

Func unmountServer()
	$cmd = "cmd /c net use " & $serverDriver & " /delete /yes"
	RunWait($cmd, "", @SW_HIDE)
EndFunc

Func onCheckUpdate()
	if $CmdLine[0] > 0 and $CmdLine[1] == "update" Then
		ConsoleWrite("checking update" & @CRLF)
		return True
	Else
		return False
	EndIf
EndFunc

Func checkUpdate()
	mountServer()
	Local $versionFile = config("file", "version")
	Local $newVersion = FileReadLine($serverDriver & $serverProgPath & $versionFile, 1)

	Local $oldVersion = FileReadLine($progDir & $versionFile, 1)
	if StringCompare($newVersion, $oldVersion, 0) > 0 Then
		startUpdate()
	EndIf
	unmountServer()
EndFunc

Func startUpdate()
	; debug use
	; DirRemove($workDir, 1)

	; display updating
	GUICreate(config("txt", "title"), 200, 100)
	GUICtrlCreateLabel(config("txt", "loading"), 10, 10)
	GUISetState(@SW_SHOW)
	GUICtrlCreateLabel(config("txt", "updating"), 10, 50)

	DirCreate($workDir)
	Local $result = DirCopy($serverDriver & $serverProgPath, $newProgDir, $FC_OVERWRITE)
EndFunc


Func Main()
	prepareConfig()

	if onCheckUpdate() Then
		checkUpdate()
	EndIf

	; auto download program for the first time
	If not FileExists($progDir) Then
		mountServer()
		startUpdate()
		unmountServer()
	EndIf

	; if downloaded update program, use it
	if FileExists($newProgDir) Then
		$result = DirRemove($progDir, 1)
		DirMove($newProgDir, $progDir, $FC_OVERWRITE)
		Local $afterInstall = $progDir & config("config", "afterInstall")
		RunWait($afterInstall, "", @SW_HIDE)
	EndIf

    ; run program now!
	$cmd = $progDir & config("file", "run")
	Run($cmd)
EndFunc

; script start here
Main()